
// TODO 3.4 - Použijte dědičnost
export class Operation {
  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

}

export class Add {
  constructor() {
    // TODO 3.5 - Zavolejte konstruktor rodiče
    this.name = "add";
  }

  calculate(a, b) {
    return a + b;
  };
}

export default class Calculator {
  constructor() {
    this.operations = {};
  }

  addOperation(operation) {
    this.operations[operation.name] = operation;
  };

  calculate(name, a, b) {
    return this.operations[name].calculate(a, b);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
// TODO 3.3 - Přidejte akci odčítání

