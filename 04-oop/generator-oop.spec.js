describe('Generator', function() {
  it('může být instancován', function() {
    expect(typeof new Generator()).toBe('object');
    expect(new Generator() instanceof Generator).toBeTruthy();
  });

  it('má metodu increase', function() {
    var generator = new Generator();
    expect(typeof generator.increase).toBe("function");
  });

  it('při volání zvyšuje hodnotu o 1', function() {
    var generator = new Generator();

    var first = generator.increase();
    var second = generator.increase();
    expect(second - first).toBe(1);
  });

  it('při prvním volání vrací iniciální hodnotu', function() {
    var generator = new Generator(1);
    expect(generator.increase()).toBe(1);
  });

  it('má defaultní iniciální hodnotu 0', function() {
    var generator = new Generator();
    expect(generator.increase()).toBe(0);
  });

  it('používá prototype', function() {
    var generator = new Generator();
    var generator2 = new Generator();
    expect(generator.increase).toBeDefined();
    expect(generator.increase).toBe(generator2.increase);
  });

  it('byl instancován do proměnné generatorFrom10', function() {
    expect(generatorFrom10 instanceof Generator).toBeTruthy();
  });

  it('první generovaná hodnota je 10', function() {
    expect(firstGenerated).toBe(10);
  });
  it('další volání generátoru vrátí 11', function() {
    expect(generatorFrom10.increase()).toBe(11);
  });
});

