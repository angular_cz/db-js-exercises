function Add() {
  // TODO 3.5 - Zavolejte konstruktor rodiče
  this.name = "add";
}

// TODO 3.1 - Ošetření vstupů operace Add

Add.prototype.calculate = function(a, b) {
  return a + b;
};

// TODO 3.2 - Vytvořte operaci odčítání

// Operation

// TODO 3.4 - Použijte dědičnost
function Operation() {
  this.name = this.constructor.name.toLowerCase();
}

var calculator = new Calculator();
calculator.addOperation(new Add());
// TODO 3.3 - Přidejte akci odčítání

// --- Nemodifikujte kód níže ---------------
// Calculator -------------------------------

function Calculator() {
  this.operations = {};

  this.addOperation = function(operation) {
    this.operations[operation.name] = operation;
  };

  this.calculate = function(name, a, b) {
    return this.operations[name].calculate(a, b);
  }
}


