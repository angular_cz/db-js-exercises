// TODO 1.1 - Vytvořte generátor
export class Generator {
  constructor(initialNumber = 0) {
    this.value = initialNumber;
  }

  increase() {
    return this.value++;
  }
}

export let generatorFrom10 = new Generator(10);
export let firstGenerated = generatorFrom10.increase();


// TODO 1.2 - Použijte prototype
// TODO 1.3 - Opravte přístup k attributům

// TODO 1.4 - Instancujte Generátor

