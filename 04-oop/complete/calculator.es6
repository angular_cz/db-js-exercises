
// TODO 3.4 - Použijte dědičnost
export class Operation {
  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

  // TODO 3.1 - Ošetření vstupů operace Add
  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {

  calculateInternal_(a, b) {
    return a + b;
  };
}

// TODO 3.2 - Vytvořte operaci odčítání
export class Sub extends Operation {
  calculateInternal_(a, b) {
    return a - b;
  };
}

export default class Calculator {
  constructor() {
    this.operations = {};
  }

  addOperation(operation) {
    this.operations[operation.name] = operation;
  };

  calculate(name, a, b) {
    return this.operations[name].calculate(a, b);
  }
}

export let calculator = new Calculator();
calculator.addOperation(new Add());
calculator.addOperation(new Sub());
// TODO 3.3 - Přidejte akci odčítání

