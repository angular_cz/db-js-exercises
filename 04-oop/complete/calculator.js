// Add
function Add() {
  Operation.call(this);
}

Add.prototype = Object.create(Operation.prototype);
Add.prototype.constructor = Add;

Add.prototype.calculateInternal_ = function(a, b) {
  return a + b;
};

// Sub
function Sub() {
  Operation.call(this);
}

Sub.prototype = Object.create(Operation.prototype);
Sub.prototype.constructor = Sub;

Sub.prototype.calculateInternal_ = function(a, b) {
  return a - b;
};

// Operation
function Operation() {
  this.name = this.constructor.name.toLowerCase();
}

Operation.prototype.calculate = function(a, b) {
  return this.calculateInternal_(parseFloat(a), parseFloat(b));
};

var calculator = new Calculator();
calculator.addOperation(new Add());
calculator.addOperation(new Sub());

// --- Nemodifikujte kód níže ------

function Calculator() {
  this.operations = {};

  this.addOperation = function(operation) {
    this.operations[operation.name] = operation;
  };

  this.calculate = function(name, a, b) {
    return this.operations[name].calculate(a, b);
  }
}