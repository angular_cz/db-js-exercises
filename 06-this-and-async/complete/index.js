var Countdown = function(intervalInSeconds) {

  this.checkInterval(intervalInSeconds);

  this.defaultInterval = intervalInSeconds;

  this.remaining = null;
  this.intervalID = null;

};

Countdown.prototype.doTick = function() {
  this.remaining--;

  if (this.remaining < 0) {
    this.stop();
    this.callFinalCallback();
    return;
  }

  this.callUpdateCallback();
};

Countdown.prototype.start = function(updateCallback, finalCallback) {
  this.updateCallback = updateCallback;
  this.finalCallback = finalCallback;

  this.remaining = this.defaultInterval;

  // TODO 1.2 - Implementace odpočtu
  this.intervalID = setInterval(this.doTick.bind(this), 1000);
};

Countdown.prototype.stop = function() {

  // TODO 1.5 - Zastavte časovač
  clearInterval(this.intervalID);

  this.intervalID = null;
  this.updateCallback = null;
};

Countdown.prototype.checkInterval = function(interval) {

  if (typeof interval !== "number") {
    throw new TypeError("interval musí být číslo");
  }

  if (interval <= 0 || !isFinite(interval) || isNaN(interval)) {
    throw new RangeError("interval musí být konečné větší než 0");
  }

};

Countdown.prototype.callUpdateCallback = function() {
  if (this.updateCallback) {
    try {
      this.updateCallback(this.remaining);
    } catch (e) {
      console.error("updateCallback error: ", e);
    }
  }
};

Countdown.prototype.callFinalCallback = function() {
  if (this.finalCallback) {
    try {
      this.finalCallback(this.defaultInterval);
    } catch (e) {
      console.error("finalCallback error: ", e);
    }

    this.finalCallback = null;
  }
};

Countdown.prototype.isRunning = function() {
  return Boolean(this.intervalID);
};
