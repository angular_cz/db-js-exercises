// ## 1. Undefined / null ------------------------------------------------------------------------- ##

// 1.1 - Proměnná "defined" nesmí být undefined
var defined;

console.assert(typeof defined != 'undefined', '1.1 - Proměnná "defined" nesmí být undefined');

// 1.2 - Proměnná "nullVariable" musí být skutečně null
var nullVariable = "not-null";

console.assert(!nullVariable && typeof nullVariable == 'object', '1.2 - Proměnná "nullVariable" musí být skutečně null');




// ## 2. Boolean ---------------------------------------------------------------------------------- ##

var notBoolean = "do-not-modify";  // Nemodifikujte

// 2.1 - Upravte explicitBoolean tak, aby byla typu boolean
var explicitBoolean = notBoolean;

console.assert(notBoolean === "do-not-modify", '2.1 - Proměnnou "notBoolean" nemodifikujte');
console.assert(typeof explicitBoolean == 'boolean', '2.1 - Proměnná "explicitBoolean" musí být opravdu boolean');

// 2.2 - Upravte výrazy tak, aby byly vyhodnoceny na false
var stringToBoolean = "non-empty";
var numberToBoolean = 42;
var objectToBoolean = {};

console.assert(typeof stringToBoolean == 'string', '2.2.1 - Proměnná "stringToBoolean" musí být typu string');
console.assert(!Boolean(stringToBoolean), '2.2.1 - Proměnná "stringToBoolean" musí být vyhodnocena na false');

console.assert(typeof numberToBoolean == 'number', '2.2.2 - Proměnná "numberToBoolean" musí být typu číslo');
console.assert(!Boolean(numberToBoolean), '2.2.2 - Proměnná "numberToBoolean" musí být vyhodnocena na false');

console.assert(typeof objectToBoolean == 'object', '2.2.3 - Proměnná "objectToBoolean" musí být typu object');
console.assert(!Boolean(objectToBoolean), '2.2.3 - Proměnná "objectToBoolean" musí být vyhodnocena na false');




// ## 3. String ----------------------------------------------------------------------------------- ##

var randomNumber = Math.random();  // nemodifikujte

// 3.1 - Převeďte číslo na řetězec
var randomString = randomNumber;

console.assert(randomString == randomNumber, '3.1 - Při vyhodnocení používejte "randomNumber"');
console.assert(typeof randomString == "string", '3.1 - Proměnná "randomString" musí být řetězcem');

// 3.2 - Přiřaďte do proměnné délku řetězce randomString
var stringLength = 0;

console.assert(stringLength === randomString["\x6C\x65\x6E\x67\x74\x68"], '3.2 - Proměnná "stringLength" musí obsahovat délku řetězce "randomString"');

var firstString = "Ahoj náhodné čislo:";  // Nemodifikujte
var secondString = randomString;          // Nemodifikujte

// 3.3 - Spojte řetězce firstString a secondString
var concatenatedString = "";

console.assert(concatenatedString.indexOf(firstString) === 0 && concatenatedString.indexOf(secondString) > -1 && firstString["\x6C\x65\x6E\x67\x74\x68"] + secondString["\x6C\x65\x6E\x67\x74\x68"] == concatenatedString["\x6C\x65\x6E\x67\x74\x68"], '3.3 - Proměnná "concatenatedString", musí být spojením řetězců "firstString" a "secondString"')




// ## 4. Čísla ------------------------------------------------------------------------------------ ##

// 4.1 - Změnou operandu vytvořte nekonečno
var divisor = 1;

var infinityNumber = 10 / divisor;  // nemodifikujte

console.assert(!isFinite(infinityNumber), '4.1 - Proměnná "infinityNumber" musí být nekonečno');

// 4.2 - Opravte operand aby nebyl výpočet NaN
var operand1 = "number one";

var operand2 = 1; // nemodifikujte
var result = operand1 - operand2;  // nemodifikujte

console.assert(!isNaN(result), '4.2 - Proměnná "result" nesmí mít hodnotu NaN');

var stringOperand = "1"; // nemodifikujte

// 4.3 - Upravte použití stringOperandu, tak  aby byl výsledkem součet
var numberResult = 1 + stringOperand;

console.assert(numberResult === 2, '4.3 - Hodnota proměnné "numberResult" musí být součet');

var decimal1 = 0.2;   // nemodifikujte
var decimal2 = 0.1;   // nemodifikujte
var decimalSum = decimal1 + decimal2;  // nemodifikujte

// 4.4 - Upravte porovnání tak, aby byl výsledek true
var decimalComparison = (decimalSum == 0.3);

console.assert(decimalComparison, '4.4 - Výsledkem porovnání "decimalComparison" musí být true');

var decimalNumber = 3.14156; // nemodifikujte

// 4.5 - Ořízněte na dvě desetinná místa pomocí metody na number;
var truncatedDecimal = decimalNumber;

console.assert(truncatedDecimal === "3.14", '4.5 - Proměnná "truncatedDecimal" musí obsahovat oříznuté číslo "decimalNumber"');

// ## 5. Math ------------------------------------------------------------------------------------- ##

var decimalNumber = Math.PI; // nemodifikujte

// 5.1 - Zaokrouhlete pomocí objektu Math nahoru.
var ceiledNumber = decimalNumber;

console.assert(ceiledNumber === 4, '5.1 - Proměnná "ceiledNumber" obsahovat "decimalNumber" zaokrouhlený nahoru');

// ## 6. Porovnání -------------------------------------------------------------------------------- ##

var stringOperand = "42";  // nemodifikujte
var numberOperand = 42;    // nemodifikujte

// 6.1 - Porovnejte operandy stringOperand a numberOperand typově - musí vrátit false
var typeComparison = true;

console.assert(typeComparison === false, '6.1 - Proměnná "typeComparison" musí obsahovat typové porovnání "stringOperand" a "numberOperand"');

// 6.2 - Porovnejte operandy stringOperand a numberOperand netypově - musí vrátit true
var nonTypeComparison = false;

console.assert(nonTypeComparison === true, '6.2 - Proměnná "nonTypeComparison" musí obsahovat netypové porovnání "stringOperand" a "numberOperand"');




// ## 7. Operátory konjunkce, dizjunkce ----------------------------------------------------------- ##

// 7.1 - Modifikujte operand, tak aby byl v následujícím porovnáním vrácen poslední operand
// var orResult = operand1 || operand2 || lastOperand;
var operand1 = true;
var operand2 = false;

var lastOperand = Math.random();  // nemodifikujte
var orResult = operand1 || operand2 || lastOperand; // nemodifikujte

console.assert(orResult === lastOperand, '7.1 - Proměnná "orResult" musí obsahovat poslední operand');

// 7.2 Modifikujte operand, tak aby se assert nevykonal
var operand = true;

var andResult = operand && console.assert(false, '7.2 - tento assert vždy selže, zajistěte, aby se nevolal'); // nemodifikujte


