(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['ReatDOM', 'calculator'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('ReatDOM'), require('calculator'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.ReatDOM, global.calculator);
    global.index = mod.exports;
  }
})(this, function (_ReatDOM, _calculator) {
  'use strict';

  var _ReatDOM2 = _interopRequireDefault(_ReatDOM);

  var _calculator2 = _interopRequireDefault(_calculator);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var start = function start(mountNode) {
    ReactDOM.render(React.createElement(_calculator2.default, null), mountNode);
  };

  document.addEventListener("DOMContentLoaded", function () {
    start(document.querySelector('#app'));
  });
});