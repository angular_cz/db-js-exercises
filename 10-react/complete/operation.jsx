import React from 'React';

let Operation = (props) => {
  return <button type="button"
                 className="operation"
                 name={props.name}
                 onClick={props.onClick}>
    {props.symbol}
  </button>
};

Operation.propTypes = {
  name: React.PropTypes.string.isRequired,
  symbol: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired
};

export default Operation;