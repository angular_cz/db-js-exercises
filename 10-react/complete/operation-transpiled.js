(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "React"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("React"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.React);
    global.operation = mod.exports;
  }
})(this, function (exports, _React) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _React2 = _interopRequireDefault(_React);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Operation = function Operation(props) {
    return _React2.default.createElement("button", {
      type: "button",
      className: "operation",
      name: props.name,
      onClick: props.onClick
    }, props.symbol);
  };

  Operation.propTypes = {
    name: _React2.default.PropTypes.string.isRequired,
    symbol: _React2.default.PropTypes.string.isRequired,
    onClick: _React2.default.PropTypes.func.isRequired
  };
  exports.default = Operation;
});