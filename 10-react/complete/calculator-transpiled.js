var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'React', 'display', 'buttons', 'operations'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('React'), require('display'), require('buttons'), require('operations'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.React, global.display, global.buttons, global.operations);
    global.calculator = mod.exports;
  }
})(this, function (exports, _React, _display, _buttons, _operations) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _React2 = _interopRequireDefault(_React);

  var _display2 = _interopRequireDefault(_display);

  var _buttons2 = _interopRequireDefault(_buttons);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && ((typeof call === 'undefined' ? 'undefined' : _typeof(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var Calculator = function (_React$Component) {
    _inherits(Calculator, _React$Component);

    function Calculator() {
      _classCallCheck(this, Calculator);

      var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Calculator).call(this));

      _this.state = Calculator.initialState;
      _this.operations = {
        'plus': new _operations.Add(),
        'minus': new _operations.Sub(),
        'mul': new _operations.Mul(),
        'div': new _operations.Div()
      };
      return _this;
    }

    _createClass(Calculator, [{
      key: 'setOperand',
      value: function setOperand(operand) {
        this.setState({
          operand: operand,
          displayValue: operand
        });
      }
    }, {
      key: 'processOperation',
      value: function processOperation(type) {
        switch (type) {
          case "clear":
            this.setState(Calculator.initialState);
            break;

          case "equals":
            var result = this.calculateResult_();
            this.setState({
              result: result,
              displayValue: result
            });
            break;

          default:
            if (this.state.result === null || !this.state.operation) {
              this.setState({
                operation: type,
                result: this.state.operand
              });
            } else {
              this.setState({
                operation: type,
                operand: this.state.result
              });
            }

        }
      }
    }, {
      key: 'render',
      value: function render() {
        return _React2.default.createElement('div', {
          className: 'calculator'
        }, _React2.default.createElement(_display2.default, {
          value: this.state.displayValue
        }), _React2.default.createElement(_buttons2.default, {
          onOperandChange: this.setOperand.bind(this),
          onOperation: this.processOperation.bind(this)
        }));
      }
    }, {
      key: 'calculate_',
      value: function calculate_(name, a, b) {
        if (!this.operations[name]) {
          throw new TypeError('Unknown operation: ' + name);
        }

        return this.operations[name].calculate(a, b);
      }
    }, {
      key: 'calculateResult_',
      value: function calculateResult_() {
        if (this.state.result === null) {
          return 0;
        }

        if (this.state.operation) {
          return this.calculate_(this.state.operation, this.state.result, this.state.operand);
        }

        return this.state.result;
      }
    }]);

    return Calculator;
  }(_React2.default.Component);

  exports.default = Calculator;
  Calculator.initialState = {
    operand: null,
    result: null,
    operation: null,
    displayValue: 0
  };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNhbGN1bGF0b3IuanN4Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztNQU9xQiIsImZpbGUiOiJjYWxjdWxhdG9yLmpzeCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdSZWFjdCc7XG5cbmltcG9ydCBEaXNwbGF5IGZyb20gJ2Rpc3BsYXknXG5pbXBvcnQgQnV0dG9ucyBmcm9tICdidXR0b25zJ1xuXG5pbXBvcnQge0FkZCwgU3ViLCBNdWwsIERpdn0gZnJvbSAnb3BlcmF0aW9ucyc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIENhbGN1bGF0b3IgZXh0ZW5kcyBSZWFjdC5Db21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMuc3RhdGUgPSBDYWxjdWxhdG9yLmluaXRpYWxTdGF0ZTtcblxuICAgIHRoaXMub3BlcmF0aW9ucyA9IHtcbiAgICAgICdwbHVzJzogbmV3IEFkZCgpLFxuICAgICAgJ21pbnVzJzogbmV3IFN1YigpLFxuICAgICAgJ211bCc6IG5ldyBNdWwoKSxcbiAgICAgICdkaXYnOiBuZXcgRGl2KClcbiAgICB9XG4gIH1cblxuICBzZXRPcGVyYW5kKG9wZXJhbmQpIHtcbiAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgIG9wZXJhbmQsXG4gICAgICBkaXNwbGF5VmFsdWU6IG9wZXJhbmRcbiAgICAgIC8vIFRPRE8gMi4xIHphcGnFoXRlIG9wZXJhbmQgZG8gc3RhdGUgZGlzcGxheVZhbHVlLCBhYnkgc2Ugem9icmF6b3ZhbCBuYSBkaXNwbGVqaVxuICAgIH0pO1xuICB9XG5cbiAgcHJvY2Vzc09wZXJhdGlvbih0eXBlKSB7XG4gICAgc3dpdGNoICh0eXBlKSB7XG4gICAgICBjYXNlIFwiY2xlYXJcIjpcbiAgICAgICAgLy8gVE9ETyAyLjIgb2Jub3Z0ZSBzdGF2IGRvIGluaXRpYWxTdGF0ZVxuICAgICAgICB0aGlzLnNldFN0YXRlKENhbGN1bGF0b3IuaW5pdGlhbFN0YXRlKTtcbiAgICAgICAgYnJlYWs7XG5cbiAgICAgIGNhc2UgXCJlcXVhbHNcIjpcbiAgICAgICAgdmFyIHJlc3VsdCA9IHRoaXMuY2FsY3VsYXRlUmVzdWx0XygpO1xuICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICByZXN1bHQsXG4gICAgICAgICAgZGlzcGxheVZhbHVlOiByZXN1bHRcbiAgICAgICAgfSk7XG4gICAgICAgIGJyZWFrO1xuXG4gICAgICBkZWZhdWx0OlxuICAgICAgICBpZiAodGhpcy5zdGF0ZS5yZXN1bHQgPT09IG51bGwgfHwgIXRoaXMuc3RhdGUub3BlcmF0aW9uKSB7ICAvLyBmaXJzdCBvcGVyYXRpb25cbiAgICAgICAgICB0aGlzLnNldFN0YXRlKHtcbiAgICAgICAgICAgIG9wZXJhdGlvbjogdHlwZSxcbiAgICAgICAgICAgIHJlc3VsdDogdGhpcy5zdGF0ZS5vcGVyYW5kXG4gICAgICAgICAgfSk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGhpcy5zZXRTdGF0ZSh7XG4gICAgICAgICAgICBvcGVyYXRpb246IHR5cGUsXG4gICAgICAgICAgICBvcGVyYW5kOiB0aGlzLnN0YXRlLnJlc3VsdFxuICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuICB9XG5cbiAgcmVuZGVyKCkgeyAgLy8gVE9ETyAxLjMga29tcG9uZW50xJsgZGlzcGxheSBwxZllZMOhdmVqdGUgaG9kbm90dSB6ZSBzdGF2dVxuICAgIHJldHVybiA8ZGl2IGNsYXNzTmFtZT1cImNhbGN1bGF0b3JcIj5cbiAgICAgIDxEaXNwbGF5IHZhbHVlPXt0aGlzLnN0YXRlLmRpc3BsYXlWYWx1ZX0vPlxuICAgICAgPEJ1dHRvbnNcbiAgICAgICAgICBvbk9wZXJhbmRDaGFuZ2U9e3RoaXMuc2V0T3BlcmFuZC5iaW5kKHRoaXMpfVxuICAgICAgICAgIG9uT3BlcmF0aW9uPXt0aGlzLnByb2Nlc3NPcGVyYXRpb24uYmluZCh0aGlzKX0vPlxuICAgIDwvZGl2PlxuICB9XG5cbiAgY2FsY3VsYXRlXyhuYW1lLCBhLCBiKSB7XG4gICAgaWYgKCF0aGlzLm9wZXJhdGlvbnNbbmFtZV0pIHtcbiAgICAgIHRocm93IG5ldyBUeXBlRXJyb3IoJ1Vua25vd24gb3BlcmF0aW9uOiAnICsgbmFtZSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMub3BlcmF0aW9uc1tuYW1lXS5jYWxjdWxhdGUoYSwgYik7XG4gIH1cblxuICBjYWxjdWxhdGVSZXN1bHRfKCkge1xuICAgIGlmICh0aGlzLnN0YXRlLnJlc3VsdCA9PT0gbnVsbCkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuc3RhdGUub3BlcmF0aW9uKSB7XG4gICAgICByZXR1cm4gdGhpcy5jYWxjdWxhdGVfKHRoaXMuc3RhdGUub3BlcmF0aW9uLCB0aGlzLnN0YXRlLnJlc3VsdCwgdGhpcy5zdGF0ZS5vcGVyYW5kKTtcbiAgICB9XG5cbiAgICByZXR1cm4gdGhpcy5zdGF0ZS5yZXN1bHQ7XG4gIH1cbn1cblxuQ2FsY3VsYXRvci5pbml0aWFsU3RhdGUgPSB7XG4gIG9wZXJhbmQ6IG51bGwsXG4gIHJlc3VsdDogbnVsbCxcbiAgb3BlcmF0aW9uOiBudWxsLFxuICBkaXNwbGF5VmFsdWU6IDBcbn07XG5cbiJdfQ==