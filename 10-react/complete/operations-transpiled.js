var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports);
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports);
    global.operations = mod.exports;
  }
})(this, function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && ((typeof call === "undefined" ? "undefined" : _typeof(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + (typeof superClass === "undefined" ? "undefined" : _typeof(superClass)));
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  var Operation = function () {
    function Operation() {
      _classCallCheck(this, Operation);

      this.name = this.constructor.name.toLowerCase();
    }

    _createClass(Operation, [{
      key: "calculate",
      value: function calculate(a, b) {
        return this.calculateInternal_(parseFloat(a), parseFloat(b));
      }
    }]);

    return Operation;
  }();

  var Add = exports.Add = function (_Operation) {
    _inherits(Add, _Operation);

    function Add() {
      _classCallCheck(this, Add);

      return _possibleConstructorReturn(this, Object.getPrototypeOf(Add).apply(this, arguments));
    }

    _createClass(Add, [{
      key: "calculateInternal_",
      value: function calculateInternal_(a, b) {
        return a + b;
      }
    }]);

    return Add;
  }(Operation);

  var Sub = exports.Sub = function (_Operation2) {
    _inherits(Sub, _Operation2);

    function Sub() {
      _classCallCheck(this, Sub);

      return _possibleConstructorReturn(this, Object.getPrototypeOf(Sub).apply(this, arguments));
    }

    _createClass(Sub, [{
      key: "calculateInternal_",
      value: function calculateInternal_(a, b) {
        return a - b;
      }
    }]);

    return Sub;
  }(Operation);

  var Div = exports.Div = function (_Operation3) {
    _inherits(Div, _Operation3);

    function Div() {
      _classCallCheck(this, Div);

      return _possibleConstructorReturn(this, Object.getPrototypeOf(Div).apply(this, arguments));
    }

    _createClass(Div, [{
      key: "calculateInternal_",
      value: function calculateInternal_(a, b) {
        return a / b;
      }
    }]);

    return Div;
  }(Operation);

  var Mul = exports.Mul = function (_Operation4) {
    _inherits(Mul, _Operation4);

    function Mul() {
      _classCallCheck(this, Mul);

      return _possibleConstructorReturn(this, Object.getPrototypeOf(Mul).apply(this, arguments));
    }

    _createClass(Mul, [{
      key: "calculateInternal_",
      value: function calculateInternal_(a, b) {
        return a * b;
      }
    }]);

    return Mul;
  }(Operation);
});