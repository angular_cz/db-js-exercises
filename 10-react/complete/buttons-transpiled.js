var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['exports', 'React', 'button', 'operation'], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require('React'), require('button'), require('operation'));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.React, global.button, global.operation);
    global.buttons = mod.exports;
  }
})(this, function (exports, _React, _button, _operation) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _React2 = _interopRequireDefault(_React);

  var _button2 = _interopRequireDefault(_button);

  var _operation2 = _interopRequireDefault(_operation);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  var _createClass = function () {
    function defineProperties(target, props) {
      for (var i = 0; i < props.length; i++) {
        var descriptor = props[i];
        descriptor.enumerable = descriptor.enumerable || false;
        descriptor.configurable = true;
        if ("value" in descriptor) descriptor.writable = true;
        Object.defineProperty(target, descriptor.key, descriptor);
      }
    }

    return function (Constructor, protoProps, staticProps) {
      if (protoProps) defineProperties(Constructor.prototype, protoProps);
      if (staticProps) defineProperties(Constructor, staticProps);
      return Constructor;
    };
  }();

  function _possibleConstructorReturn(self, call) {
    if (!self) {
      throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
    }

    return call && ((typeof call === 'undefined' ? 'undefined' : _typeof(call)) === "object" || typeof call === "function") ? call : self;
  }

  function _inherits(subClass, superClass) {
    if (typeof superClass !== "function" && superClass !== null) {
      throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
    }

    subClass.prototype = Object.create(superClass && superClass.prototype, {
      constructor: {
        value: subClass,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
  }

  var Buttons = function (_React$Component) {
    _inherits(Buttons, _React$Component);

    function Buttons() {
      _classCallCheck(this, Buttons);

      var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(Buttons).call(this));

      _this.state = {
        currentNumber: null
      };
      return _this;
    }

    _createClass(Buttons, [{
      key: 'concatToSring_',
      value: function concatToSring_(value) {
        if (!this.state.currentNumber) {
          return value;
        }

        return String(this.state.currentNumber).concat(value);
      }
    }, {
      key: 'onNumberClick',
      value: function onNumberClick(value) {
        var concatenatedValue = this.concatToSring_(value);
        this.setState({
          currentNumber: concatenatedValue
        });
        this.props.onOperandChange(concatenatedValue);
      }
    }, {
      key: 'onOperationClick',
      value: function onOperationClick(event) {
        this.setState({
          currentNumber: null
        });
        this.props.onOperation(event.target.name);
      }
    }, {
      key: 'render',
      value: function render() {
        return _React2.default.createElement('div', {
          id: 'buttons'
        }, _React2.default.createElement('div', null, _React2.default.createElement(_button2.default, {
          value: 1,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 4,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 7,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_operation2.default, {
          name: 'clear',
          symbol: 'C',
          onClick: this.onOperationClick.bind(this)
        })), _React2.default.createElement('div', null, _React2.default.createElement(_button2.default, {
          value: 2,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 5,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 8,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 0,
          onClick: this.onNumberClick.bind(this)
        })), _React2.default.createElement('div', null, _React2.default.createElement(_button2.default, {
          value: 3,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 6,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_button2.default, {
          value: 9,
          onClick: this.onNumberClick.bind(this)
        }), _React2.default.createElement(_operation2.default, {
          name: 'equals',
          symbol: '=',
          onClick: this.onOperationClick.bind(this)
        })), _React2.default.createElement('div', null, _React2.default.createElement(_operation2.default, {
          name: 'plus',
          symbol: '+',
          onClick: this.onOperationClick.bind(this)
        }), _React2.default.createElement(_operation2.default, {
          name: 'minus',
          symbol: '-',
          onClick: this.onOperationClick.bind(this)
        }), _React2.default.createElement(_operation2.default, {
          name: 'mul',
          symbol: 'x',
          onClick: this.onOperationClick.bind(this)
        }), _React2.default.createElement(_operation2.default, {
          name: 'div',
          symbol: '/',
          onClick: this.onOperationClick.bind(this)
        })));
      }
    }]);

    return Buttons;
  }(_React2.default.Component);

  exports.default = Buttons;
  Buttons.propTypes = {
    onOperandChange: _React2.default.PropTypes.func.isRequired,
    onOperation: _React2.default.PropTypes.func.isRequired
  };
});