(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['React', 'calculator'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('React'), require('calculator'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.React, global.calculator);
    global.calculatorSpec = mod.exports;
  }
})(this, function (_React, _calculator) {
  'use strict';

  var _React2 = _interopRequireDefault(_React);

  var _calculator2 = _interopRequireDefault(_calculator);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  describe('Calculator', function () {
    var render = function render(element) {
      var renderer = _React2.default.addons.TestUtils.createRenderer();

      renderer.render(element);
      return renderer.getRenderOutput();
    };

    it('je definovaný', function () {
      expect(_calculator2.default).toBeDefined();
    });
    it('zapisuje stisknutá tlačítka na display (TODO 2.1)', function () {
      var dom = _React2.default.addons.TestUtils.renderIntoDocument(_React2.default.createElement(_calculator2.default, null));

      var buttons = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');

      var firstButton = buttons[0];

      var input = _React2.default.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      expect(input.value).toEqual(firstButton.value);

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      expect(input.value).toEqual(firstButton.value + firstButton.value);
    });
    it('počítá součet', function () {
      var filterByName = function filterByName(name) {
        return function (button) {
          return button.name === name;
        };
      };

      var dom = _React2.default.addons.TestUtils.renderIntoDocument(_React2.default.createElement(_calculator2.default, null));

      var input = _React2.default.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

      var buttons = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');

      var firstButton = buttons[0];

      var operations = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');

      var plusButton = operations.filter(filterByName('plus'))[0];
      var equalsButton = operations.filter(filterByName('equals'))[0];

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      _React2.default.addons.TestUtils.Simulate.click(plusButton);

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      _React2.default.addons.TestUtils.Simulate.click(equalsButton);

      expect(input.value).toEqual(String(parseInt(firstButton.value) + parseInt(firstButton.value)));
    });
    it('aplikuje operaci znovu při dalším stisku rovná se', function () {
      var filterByName = function filterByName(name) {
        return function (button) {
          return button.name === name;
        };
      };

      var dom = _React2.default.addons.TestUtils.renderIntoDocument(_React2.default.createElement(_calculator2.default, null));

      var input = _React2.default.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

      var buttons = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');

      var firstButton = buttons[0];

      var operations = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');

      var plusButton = operations.filter(filterByName('plus'))[0];
      var equalsButton = operations.filter(filterByName('equals'))[0];

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      _React2.default.addons.TestUtils.Simulate.click(plusButton);

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      _React2.default.addons.TestUtils.Simulate.click(equalsButton);

      expect(input.value).toEqual(String(2 * parseInt(firstButton.value)));

      _React2.default.addons.TestUtils.Simulate.click(equalsButton);

      expect(input.value).toEqual(String(3 * parseInt(firstButton.value)));
    });
    it('nuluje display po kliknutí na clear (TODO 2.2)', function () {
      var filterByName = function filterByName(name) {
        return function (button) {
          return button.name === name;
        };
      };

      var dom = _React2.default.addons.TestUtils.renderIntoDocument(_React2.default.createElement(_calculator2.default, null));

      var buttons = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithClass(dom, 'number-button');

      var firstButton = buttons[0];

      var operations = _React2.default.addons.TestUtils.scryRenderedDOMComponentsWithTag(dom, 'button');

      var clearButton = operations.filter(filterByName('clear'))[0];

      var input = _React2.default.addons.TestUtils.findRenderedDOMComponentWithTag(dom, 'input');

      _React2.default.addons.TestUtils.Simulate.click(firstButton);

      expect(input.value).not.toEqual('0');

      _React2.default.addons.TestUtils.Simulate.click(clearButton);

      expect(input.value).toEqual('0');
    });
  });
});