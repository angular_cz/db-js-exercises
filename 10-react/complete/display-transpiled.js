(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "React"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("React"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.React);
    global.display = mod.exports;
  }
})(this, function (exports, _React) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _React2 = _interopRequireDefault(_React);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Display = function Display(props) {
    return _React2.default.createElement("div", {
      className: "display"
    }, _React2.default.createElement("input", {
      type: "number",
      value: props.value,
      readOnly: true
    }));
  };

  Display.propTypes = {
    value: _React2.default.PropTypes.oneOfType([_React2.default.PropTypes.string, _React2.default.PropTypes.number]).isRequired
  };
  exports.default = Display;
});