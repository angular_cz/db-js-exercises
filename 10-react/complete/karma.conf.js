var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('bower_components/react/react-with-addons.js');
  config.files.push('10-react/complete/button-transpiled.js');
  config.files.push('10-react/complete/operation-transpiled.js');
  config.files.push('10-react/complete/buttons-transpiled.js');
  config.files.push('10-react/complete/display-transpiled.js');
  config.files.push('10-react/complete/operations-transpiled.js');
  config.files.push('10-react/complete/calculator-transpiled.js');
  config.files.push('10-react/complete/display.spec-transpiled.js');
  config.files.push('10-react/complete/calculator.spec-transpiled.js');

};