(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(['React', 'display'], factory);
  } else if (typeof exports !== "undefined") {
    factory(require('React'), require('display'));
  } else {
    var mod = {
      exports: {}
    };
    factory(global.React, global.display);
    global.displaySpec = mod.exports;
  }
})(this, function (_React, _display) {
  'use strict';

  var _React2 = _interopRequireDefault(_React);

  var _display2 = _interopRequireDefault(_display);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  describe('Display', function () {
    var render = function render(element) {
      var renderer = _React2.default.addons.TestUtils.createRenderer();

      renderer.render(element);
      return renderer.getRenderOutput();
    };

    it('je definovaný', function () {
      expect(_display2.default).toBeDefined();
    });
    it('obsahuje input typu number (TODO 1.1.1)', function () {
      var result = render(_React2.default.createElement(_display2.default, {
        value: 1
      }));
      expect(result.type).toBe('div');
      var children = result.props.children;
      expect(children.type).toEqual('input');
    });
    it('má definované propTypes pro value (TODO 1.2)', function () {
      expect(_display2.default.propTypes).toBeDefined();
      expect(_display2.default.propTypes.value).toBeDefined();
    });
    describe('input', function () {
      var input;
      beforeEach(function () {
        var result = render(_React2.default.createElement(_display2.default, {
          value: 111
        }));
        input = result.props.children;
      });
      it('je pouze ke čtení (TODO 1.1.2)', function () {
        expect(input.props.readOnly).toBeDefined();
      });
      it('obahuje hodnotu předanou jako props.value (TODO 1.1.3)', function () {
        expect(input.props.value).toBe(111);
      });
    });
  });
});