(function (global, factory) {
  if (typeof define === "function" && define.amd) {
    define(["exports", "React"], factory);
  } else if (typeof exports !== "undefined") {
    factory(exports, require("React"));
  } else {
    var mod = {
      exports: {}
    };
    factory(mod.exports, global.React);
    global.button = mod.exports;
  }
})(this, function (exports, _React) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  var _React2 = _interopRequireDefault(_React);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  var Button = function Button(props) {
    var onClick = function onClick() {
      props.onClick(props.value);
    };

    return _React2.default.createElement("button", {
      type: "button",
      className: "number-button",
      value: props.value,
      onClick: onClick
    }, props.value);
  };

  Button.propTypes = {
    value: _React2.default.PropTypes.number.isRequired,
    onClick: _React2.default.PropTypes.func.isRequired
  };
  exports.default = Button;
});