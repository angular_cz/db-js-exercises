import ReactDOM from 'ReactDOM'
import Calculator from 'calculator'

let start = function(mountNode) {
  ReactDOM.render(<Calculator/>, mountNode);
};

document.addEventListener("DOMContentLoaded", function() {
  start(document.querySelector('#app'));
});