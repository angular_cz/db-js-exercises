var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('bower_components/react/react-with-addons.js');
  config.files.push('10-react/button-transpiled.js');
  config.files.push('10-react/operation-transpiled.js');
  config.files.push('10-react/buttons-transpiled.js');
  config.files.push('10-react/display-transpiled.js');
  config.files.push('10-react/operations-transpiled.js');
  config.files.push('10-react/calculator-transpiled.js');
  config.files.push('10-react/display.spec-transpiled.js');
  config.files.push('10-react/calculator.spec-transpiled.js');

};

