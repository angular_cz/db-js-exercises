var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('07-async-promise/timeout-promise.js');
  config.files.push('07-async-promise/timeout-promise.spec.js');

};

