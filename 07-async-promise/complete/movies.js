var MovieService = function() {
  this.movies = null;
  this.moviesPromise = null;
};

MovieService.prototype.getMovies = function() {

  if (this.movies) {
    return Promise.resolve(this.movies);
  }
  if (!this.moviesPromise) {
    this.moviesPromise = this.loadMovies_();
  }

  return this.moviesPromise;
};

MovieService.prototype.loadMovies_ = function() {
  return fetchMovies().then(function(movies) {
    this.movies = movies;
    this.moviesPromise = null;

    return movies;
  }.bind(this));
};

MovieService.prototype.getBestMovies = function(count) {
  return this.getMovies().then(function(movies) {
    return movies.slice(0, count);
  });
};

// Načtení dat ze serveru -------------------------------------------------------------------------

function fetchMovies() {
  return fetch("/api/movies").then(function(response) {
    return response.json();
  });
}
