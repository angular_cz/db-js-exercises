var TimeoutPromise = function(delay, conditionFunction) {

  var executor = function(resolve, reject) {
    setTimeout(function() {
      var value = conditionFunction();
      if (value) {
        resolve(value);
      } else {
        reject(value);
      }
    }, delay);
  };

  return new Promise(executor);
};