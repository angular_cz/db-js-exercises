var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('07-async-promise/complete/timeout-promise.js');
  config.files.push('07-async-promise/complete/timeout-promise.spec.js');

};

