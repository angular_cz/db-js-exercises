describe('TimeoutPromise', function() {

  jasmine.DEFAULT_TIMEOUT_INTERVAL = 150;

  describe('je definována jako funkce', function() {

    it('se dvěma parametry', function() {
      expect(TimeoutPromise.length).toBe(2);
    });

    it("která vrací promise (TODO 1.1)", function() {

      var timeout = new TimeoutPromise(1, function() {
      });
      expect(timeout instanceof Promise).toBe(true);
    });

  });

  it("hodnota je stejná jako návratová hodnota conditionFunction (TODO 1.2)", function(done) {

    var conditionFunction = function() {
      return 42;
    };

    var timeout = new TimeoutPromise(1, conditionFunction);

    timeout.then(function(value) {
      expect(value).toBe(42);
      done();
    });
  });

  it("pokud funkce vrátí null je promise rejected (TODO 1.3)", function(done) {
    var conditionFunction = function() {
      return null;
    };

    var timeout = new TimeoutPromise(1, conditionFunction);

    timeout.catch(function(value) {
      expect(value).toBe(null);
      done();
    });
  });

  describe("conditionFunction", function() {

    beforeEach(function() {
      jasmine.clock().install();
    });

    afterEach(function() {
      jasmine.clock().uninstall();
    });

    it("je volána s definovaným zpožděním (TODO 1.4)", function() {

      var wasCalled = false;

      var conditionFunction = function() {
        wasCalled = true;
      };

      var timeout = new TimeoutPromise(100, conditionFunction);

      expect(wasCalled).toBe(false);

      jasmine.clock().tick(99);

      expect(wasCalled).toBe(false);

      jasmine.clock().tick(10);

      expect(wasCalled).toBe(true);

    });

  })

});