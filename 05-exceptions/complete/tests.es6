import {Calculator, Operation, UnknownOperationError} from 'calculator'

describe('Kalkulačka', () => {

  it('může být instancována', () => {
    expect((new Calculator()) instanceof Calculator).toBeTruthy();
  });

  it('může mít přidánu operaci pod jménem', () => {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    calc.addOperation(operation);

    expect(calc.operations['op']).toBe(operation);
  });

  it('umožuje provolat operaci', () => {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    spyOn(operation, "calculate").and.returnValue(42);

    calc.addOperation(operation);

    expect(calc.calculate('op', 40, 2)).toBe(42);
    expect(operation.calculate).toHaveBeenCalledWith(40, 2);
  });

  describe('vyhodí výjimku', () => {
    it('TypeError, při pokusu o přidání akce, která není potomkem Operation (TODO 1)', () => {

      // TODO 1.1 - Vytvořte test očekávající vyhození TypeError
      expect(() => {
        var calculator = new Calculator();
        calculator.addOperation({});
      }).toThrowError(TypeError);
    });

    it('UnknownOperationError, při volání neznámé operace (TODO 3)', () => {
      var calculator = new Calculator();
      expect(() => {
        calculator.calculate("unknown")
      }).toThrowError(UnknownOperationError);
    });

    it('s názvem operace, při volání neznámé operace (TODO 3)', () => {
      var calculator = new Calculator();

      function throwFunction() {
        calculator.calculate("unknown-op")
      }

      expect(throwFunction).toThrowError(UnknownOperationError);
      expect(throwFunction).toThrowError(/unknown-op/);
    });
  });

});

describe('Výjimka UnknownOperationError (TODO 2)', () => {
  it('může být instancována', () => {
    var error = new UnknownOperationError();
console.log(error);
    expect(error instanceof UnknownOperationError).toBeTruthy();
  });

  it('má správný name', () => {
    var error = new UnknownOperationError();

    expect(error.name).toBe('UnknownOperationError');
  });

  it('má přiřazený stack', () => {
    var error = new UnknownOperationError();

    expect(error.stack).toBeDefined();
  });

  it('je potomkem Error', () => {
    var error = new UnknownOperationError();

    expect(error instanceof Error).toBeTruthy();
  });

  it('obsahuje předanou message', () => {
    var throwFunction = () => {
      throw new UnknownOperationError("my message");
    };

    expect(throwFunction).toThrowError(UnknownOperationError, "my message");
  });

  it('má svůj konstruktor', () => {
    var error = new UnknownOperationError();
    expect(error.constructor).toBe(UnknownOperationError);
  });

});