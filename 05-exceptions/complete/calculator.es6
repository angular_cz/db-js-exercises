export class Operation {
  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {
  calculateInternal_(a, b) {
    return a + b;
  };
}

// TODO 2 - Vytvořte výjimku UnknownOperationError
// zde nepoužívejte extends, ale zápis s funkcí a prototypem
export let UnknownOperationError = function(message) {
  this.name = 'UnknownOperationError';
  this.message = message || 'Unknown operation';
  this.stack = (new Error()).stack;
};

UnknownOperationError.prototype = Object.create(Error.prototype);
UnknownOperationError.prototype.constructor = UnknownOperationError;

export class Calculator {
  constructor() {
    this.operations = {};
  }

  addOperation(operation) {
    // TODO 1.2 - Ošetření přidání neplatné operace
    if (!(operation instanceof Operation)) {
      throw new TypeError("operation must be instance of Operation");
    }

    this.operations[operation.name] = operation;
  }

  calculate(name, a, b) {
    // TODO 3 - Ošetření neznámé operace
    if (!this.operations[name]) {
      throw new UnknownOperationError('Unknown operation: ' + name);
    }

    return this.operations[name].calculate(a, b);
  }
}


