export class Operation {
  constructor() {
    this.name = this.constructor.name.toLowerCase();
  }

  calculate(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };
}

export class Add extends Operation {
  calculateInternal_(a, b) {
    return a + b;
  };
}

// TODO 2 - Vytvořte výjimku UnknownOperationError
// zde nepoužívejte extends, ale zápis s funkcí a prototypem
export let UnknownOperationError;

export class Calculator {
  constructor() {
    this.operations = {};
  }

  addOperation(operation) {
    // TODO 1.2 - Ošetření přidání neplatné operace

    this.operations[operation.name] = operation;
  }

  calculate(name, a, b) {
    // TODO 3 - Ošetření neznámé operace

    return this.operations[name].calculate(a, b);
  }
}


