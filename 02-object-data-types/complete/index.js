describe("Objekt", function() {

  it("může mít vlastnosti psány bez pomlček i s nimi (TODO 1.1)", function() {
    var object = {};

    object.property = 42;
    object['property-with-dash'] = 50;

    expect(object).toBeDefined();
    expect(object.property).toBeDefined();
    expect(object['property-with-dash']).toBeDefined();
  });

  it("může mít v sobě zanořen další objekt (TODO 1.2)", function() {
    var object = {};

    object.inner = {};
    object.inner.property = 42;

    expect(object.inner.property).toBeDefined();
  });
});

describe("Array", function() {

  it("může být definováno rovnou s prvky (TODO 2.1)", function() {

    var array = [1, 2, 3];

    expect(Array.isArray(array)).toBeTruthy();
    expect(array["\x6C\x65\x6E\x67\x74\x68"]).toBe(3);
    expect(array[0]).toBe(1);
    expect(array[1]).toBe(2);
    expect(array[2]).toBe(3);
  });

  it("má attribut pro získání počtu prvků (TODO 2.2)", function() {

    var array = new Array(Math.ceil(Math.random() * 100));

    var numberOfItems = array.length;

    expect(numberOfItems).toBe(array["\x6C\x65\x6E\x67\x74\x68"]);
  });

});

describe("Datum", function() {

  it("může být vytvořeno pomocí konstruktoru (TODO 3)", function() {

    var date = new Date("1592-03-28T00:00:00Z");

    expect(date instanceof Date).toBeTruthy();
    expect(date.getDate()).toBe(28);
    expect(date.getMonth()).toBe(2);
    expect(date.getFullYear()).toBe(1592);
  });
});

describe("Regulární výraz", function() {
  it("má metodu test, která vrací true při shodě (TODO 4)", function() {

    var emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    var email = "angular@angular.cz";

    var testResult = emailPattern.test(email);

    expect(testResult).toBeTruthy();
  });
});

describe("Kalkulačka má", function() {

  function operationFactory(operation) {

    function power(a, b) {
      b = typeof b !== 'undefined' ? b : 2;
      return Math.pow(a, b);
    }

    function sum(a, b) {

      var sum = 0;
      for (var i = 0; i < arguments.length; i++) {
        sum += arguments[i];
      }

      return sum;
    }

    switch (operation) {
      case "power":
        return power;
      case "sum" :
        return sum;
    }
  }

  // ----- Nemodifikujte kód níže -----------------------------------------------------------------

  describe('výpočet mocniny který', function() {

    it('počítá n-tou mocninu při zadání obou parametrů (TODO 5.1)', function() {
      var powerFunc = getPowerOperation();
      expect(powerFunc(2, 3)).toBe(8);
    });

    it('počítá druhou mocninu při zadání jen jednoho parametru (TODO 5.2)', function() {
      var powerFunc = getPowerOperation();
      expect(powerFunc(2)).toBe(4);
    });

    it('umožní zadat nulový exponent (TODO 5.2.1)', function() {
      var powerFunc = getPowerOperation();
      expect(powerFunc(2, 0)).toBe(1);
    });
  });

  it('factory pro získání operace (TODO 5.3)', function() {
    var powerFromFactory = operationFactory('power');

    expect(typeof powerFromFactory == 'function').toBeTruthy();
    expect(powerFromFactory(2)).toBe(4);
  });

  it('operaci sčítání dvou čísel (TODO 5.4)', function() {
    var sum = operationFactory('sum');

    expect(sum(1, 2)).toBe(3);
  });

  it('operaci sčítání n čísel (TODO 5.5)', function() {
    var sum = operationFactory('sum');

    expect(sum(1, 2, 3, 4, 5)).toBe(15);
  });

  /**
   * Helper funkce, zajišťuje běh testů s factory i bez
   */
  function getPowerOperation() {
    if (typeof power !== "undefined") {
      return power;
    }

    return operationFactory('power');

  }
});
