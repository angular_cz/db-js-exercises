var App = App || {};

App.Calculations = (function() {

  function Add() {
    Operation.call(this);
  }

  Add.prototype = Object.create(Operation.prototype);
  Add.prototype.constructor = Add;

  Add.prototype.calculateInternal_ = function(a, b) {
    return a + b;
  };

  function Sub() {
    Operation.call(this);
  }

  Sub.prototype = Object.create(Operation.prototype);
  Sub.prototype.constructor = Sub;

  Sub.prototype.calculateInternal_ = function(a, b) {
    return a - b;
  };

  function Div() {
    Operation.call(this);
  }

  Div.prototype = Object.create(Operation.prototype);
  Div.prototype.constructor = Div;

  Div.prototype.calculateInternal_ = function(a, b) {
    return a / b;
  };

  function Mul() {
    Operation.call(this);
  }

  Mul.prototype = Object.create(Operation.prototype);
  Mul.prototype.constructor = Mul;

  Mul.prototype.calculateInternal_ = function(a, b) {
    return a * b;
  };

  function Operation() {
    this.name = this.constructor.name.toLowerCase();
  }

  Operation.prototype.calculate = function(a, b) {
    return this.calculateInternal_(parseFloat(a), parseFloat(b));
  };

  function Calculator() {
    this.operations = {};
    this.result = null;
    this.operand = null;
    this.operation = null;

    this.addOperation = function(operation) {
      this.operations[operation.name] = operation;
    };

    this.calculate = function(name, a, b) {
      if (!this.operations[name]) {
        throw new TypeError('Unknown operation: ' + name);
      }

      return this.operations[name].calculate(a, b);
    }

    this.equals = function() {
      if (this.result === null) {
        this.result = 0;
      }

      if (this.operation) {
        this.result = this.calculate(this.operation, this.result, this.operand);
      }

      return this.result;
    }

    this.setOperand = function(operand) {
      this.operand = operand;
      if (this.result === null || !this.operation) {
        this.result = operand;
      }
    }

    this.setOperation = function(operation) {
      if (!this.operations[operation]) {
        throw new TypeError('Unknown operation: ' + operation);
      }

      this.operation = operation;
    }

    this.clear = function() {
      this.result = null;
      this.operand = null;
      this.operation = null;
    }
  }

  return {
    Add: Add,
    Sub: Sub,
    Mul: Mul,
    Div: Div,
    Calculator: Calculator
  }

})();
