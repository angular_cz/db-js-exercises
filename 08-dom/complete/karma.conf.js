var baseConfig = require('../../base-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);

  config.set({
    basePath: '../../'
  });

  config.files.push('08-dom/complete/calculator.js');
  config.files.push('08-dom/complete/display.js');
  config.files.push('08-dom/complete/buttonControls.js');

  config.files.push('08-dom/complete/calculator.spec.js');
  config.files.push('08-dom/complete/display.spec.js');
  config.files.push('08-dom/complete/buttonControls.spec.js');
};

