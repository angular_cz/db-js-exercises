describe('Display', function() {
  beforeEach(function() {
    this.element = jasmine.createSpyObj("element", ['setAttribute']);
  })

  it('renderuje data do elementu', function() {
    var display = new App.Display(this.element);

    expect(this.element.setAttribute).toHaveBeenCalledWith("value", "0");
  });
});
