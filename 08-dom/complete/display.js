var App = App || {};

App.Display = (function() {
  var Display = function(displayElement) {

    this.isResult_ = false;
    this.element_ = displayElement;  // check
    this.value_ = null;

    this.setValue = function(value) {
      this.isResult_ = true;
      this.value_ = value;
      this.render_();
    };

    this.addToValue = function(value) {
      if (this.isResult_) {
        this.setValue("");
        this.isResult_ = false;
      }

      this.value_ += value;
      this.render_();
    };

    this.getValue = function() {
      return this.value_;
    };

    this.clear = function() {
      this.setValue("0");
    };

    this.render_ = function() {
      this.element_.setAttribute("value", this.value_);
    };

    this.clear();
  };

  return Display;
})();
