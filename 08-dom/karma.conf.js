var baseConfig = require('../base-karma.conf.js');
var courseWareConfig = require('../courseware-karma.conf.js');

module.exports = function(config) {
  baseConfig(config);
  courseWareConfig(config);

  config.set({
    basePath: '../'
  });

  config.files.push('08-dom/calculator.js');
  config.files.push('08-dom/display.js');
  config.files.push('08-dom/buttonControls.js');
  config.files.push('08-dom/calculator.spec.js');
  config.files.push('08-dom/display.spec.js');
  config.files.push('08-dom/buttonControls.spec.js');
};

