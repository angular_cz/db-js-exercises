(function() {
  var generatorFactory = function(initialNumber) {

    var step = 5;
    var value = initialNumber || 0;

    function setStep(newStep) {
      step = newStep;
    }

    function increase() {
      value += step;
      return value;
    }

    return {
      setStep: setStep,
      increase: increase
    };

  };

  window.stepGeneratorFactory = generatorFactory;
})();

