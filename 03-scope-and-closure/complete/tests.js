describe("generatorFactory", function() {
  var baseGeneratorFactory = getBaseGeneratorFactory();

  it("přijímá jeden parametr - initialNumber (TODO 1.1)", function() {
    expect(baseGeneratorFactory.length).toBe(1);
  });

  it("je funkce která vrací jinou funkci (TODO 1.2)", function() {
    var returnValueType = typeof baseGeneratorFactory();

    expect(returnValueType).toBe("function");
  });

  it("bez parametrů vrací funkci, která vrátí 0 při prvním volání (TODO 1.2)", function() {
    var generator = baseGeneratorFactory();

    expect(generator()).toBe(0);
  });

});

describe("generator", function() {
  var baseGeneratorFactory = getBaseGeneratorFactory();

  it("vrací při prvním volání initialNumber factory, ze které vznikl (TODO 1.2)", function() {
    var generatorFromTen = baseGeneratorFactory(10);

    expect(generatorFromTen()).toBe(10);
  });

  it("vrací zvýšené číslo při každém dalším volání (TODO 1.3)", function() {
    var generatorFromTen = baseGeneratorFactory(100);

    expect(generatorFromTen()).toBe(100);
    expect(generatorFromTen()).toBe(101);
    expect(generatorFromTen()).toBe(102);
    expect(generatorFromTen()).toBe(103);
  });

});

describe("IIFE a globální proměnné", function() {

  it("baseGeneratorFactory je dostupná", function() {
    expect(window.baseGeneratorFactory).toBeDefined();
  });

  it("stepGeneratorFactory je dostupná", function() {
    expect(window.stepGeneratorFactory).toBeDefined();
  });

  it("generatorFactory už není globální proměnná", function() {
    expect(window.generatorFactory).not.toBeDefined();
  });

});

describe("stepGeneratorFactory", function() {

  it("vrací objekt s vlasnostmi setStep a increase", function() {
    var generator = stepGeneratorFactory();

    expect(generator.setStep).toBeDefined();
    expect(generator.increase).toBeDefined();
  });

  it("setStep je funkce s jedním parametrem", function() {
    var generator = stepGeneratorFactory();

    expect(typeof generator.setStep).toBe("function");
    expect(generator.setStep.length).toBe(1);
  });

  it("increase je funkce, vrací hodnotu zvýšenou o step", function() {
    var generator = stepGeneratorFactory(0);

    expect(typeof generator.increase).toBe("function");

    generator.setStep(10);

    expect(generator.increase()).toBe(10);
    expect(generator.increase()).toBe(20);
  });

});

/**
 * Helper funkce, zajišťuje běh testů s IIFE i bez
 */
function getBaseGeneratorFactory() {
  if (typeof window.baseGeneratorFactory === "undefined") {
    return generatorFactory;
  } else {
    return window.baseGeneratorFactory;
  }
}